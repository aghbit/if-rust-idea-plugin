# [WIP] Rustidea
[![Build Status](https://travis-ci.org/jajakobyly/rustidea.svg?branch=master)](https://travis-ci.org/jajakobyly/rustidea)

Rustidea turns [IntelliJ IDEA](http://www.jetbrains.com/idea/) into a (not yet) convenient [Rust](https://www.rust-lang.org/) IDE.
